#!/bin/bash

set -e

# The testing dir using ctest is tightly bound to the cmake build
# but testing/install runs independently

DEB_HOST_MULTIARCH=$(dpkg-architecture -qDEB_HOST_MULTIARCH)
DEB_HOST_ARCH_ENDIAN=$(dpkg-architecture -qDEB_HOST_ARCH_ENDIAN)


declare -a EXTRA_CTEST_FLAGS

# EncryptionOperator (adios_plugin_operator*) fails on big-endian systems
# where -DADIOS2_USE_Endian_Reverse=ON (s390x, ppc64 etc)
if [ "x${DEB_HOST_ARCH_ENDIAN}" = "xbig" ]; then
  EXTRA_CTEST_FLAGS=(${EXTRA_CTEST_FLAGS[@]} -E "adios_plugin_operator*" --no-tests=ignore)
fi

if [ "x${EXTRA_CTEST_FLAGS}" != "x" ]; then
  echo "testing with EXTRA_CTEST_FLAGS=${EXTRA_CTEST_FLAGS[@]}"
fi

cd testing/install

# install tests must be run separately, see upstream Issue#3795
# We don't have paraview catalyst, so exclude the Catalyst test.
for testdir in `find . -mindepth 1 -maxdepth 1 -name CatalystEnginePlugin -prune -o -type d -print`; do
  echo Testing $testdir
  cd $testdir
  mkdir build
  cd build
  cmake -DCMAKE_PREFIX_PATH=/usr/lib/${DEB_HOST_MULTIARCH}/cmake/adios2/serial  ..
  make
  ADIOS2_PLUGIN_PATH=/usr/lib/${DEB_HOST_MULTIARCH}/adios2/serial/plugins ctest --output-on-failure "${EXTRA_CTEST_FLAGS[@]}"
  cd ../..
done
